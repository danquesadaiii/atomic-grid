import React from 'react';

import { StyledDiv2 } from '../../global-styles';
import { withProfiler } from '../../hoc/withProfiler';

import { getAppConfig } from '../../hooks';

const OtherComponent = () => {
  const {
    data: reactQueryData,
    error,
    isLoading,
    isError,
  } = getAppConfig({
    refetch: false,
    id: 'otherComponent',
  });

  return (
    <StyledDiv2>
      <h3>Another Component</h3>
      {JSON.stringify(reactQueryData, null, 2)}
    </StyledDiv2>
  );
};

export default withProfiler(OtherComponent, '#1abc9c');
