import React, { useMemo } from 'react';
import { useSelector } from 'react-redux';

import Table from './Table';

import { makeData } from '../../utils';
import { getAppConfig } from '../../hooks';

import { RootState } from '../../global-state/root-reducer';
import { StyledDiv1 } from '../../global-styles';
import { withProfiler } from '../../hoc/withProfiler';

const Grid = () => {
  const { got_application_config } = useSelector(
    (state: RootState) => state.atomicGridReducer
  );

  const data = useMemo(() => makeData(5), []);

  const {
    data: reactQueryData,
    error,
    isLoading,
    isError,
  } = getAppConfig({
    refetch: true,
    id: 'grid',
  });

  if (isLoading) {
    return <div style={{ fontSize: 100 }}>Loading (test)...</div>;
  }
  if (isError) {
    return <div>Error! {error.message}</div>;
  }

  const { columns: appConfigColumns } = reactQueryData ?? {};

  return (
    <StyledDiv1>
      <h3>Test Grid</h3>
      <i>
        Possibly another component aside from the table but belongs to Grid.
      </i>
      <hr />
      <Table columns={appConfigColumns} data={data} />
    </StyledDiv1>
  );
};

export default withProfiler(Grid, '#333');
