export enum ApplicationConfig {
  GET_APPLICATION_CONFIG = 'GET_APPLICATION_CONFIG',
  GET_APPLICATION_CONFIG_SUCCESS = 'GET_APPLICATION_CONFIG_SUCCESS',
  GET_APPLICATION_CONFIG_FAILURE = 'GET_APPLICATION_CONFIG_FAILURE',
}
