import { ApplicationConfig } from './types';

const INITIAL_STATE = {
  fetching_application_config: false,
  got_application_config: false,
  got_application_config_error: false,
  app_config: {},
};

const atomicGridReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case ApplicationConfig.GET_APPLICATION_CONFIG:
      return {
        ...state,
        fetching_application_config: true,
      };
    case ApplicationConfig.GET_APPLICATION_CONFIG_SUCCESS:
      return {
        ...state,
        fetching_application_config: false,
        got_application_config: true,
      };
    case ApplicationConfig.GET_APPLICATION_CONFIG_FAILURE:
      return {
        ...state,
        fetching_application_config: false,
        got_application_config: false,
        got_application_config_error: true,
      };
    default:
      return state;
  }
};

export default atomicGridReducer;
