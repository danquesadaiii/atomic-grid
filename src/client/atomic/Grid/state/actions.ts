import { ApplicationConfig } from './types';

export const GetApplicationConfig = () => ({
  type: ApplicationConfig.GET_APPLICATION_CONFIG,
});
