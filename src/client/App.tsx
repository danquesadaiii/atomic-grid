import React from 'react';
import { Route, Switch, BrowserRouter } from 'react-router-dom';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import { QueryClient, QueryClientProvider } from 'react-query';

import { store, persistor } from './global-state';

import Home from './pages/Home';

import './App.css';

const queryClient = new QueryClient({
  defaultOptions: {
    queries: {
      refetchOnWindowFocus: false,
    },
  },
});

const App = () => (
  <Provider store={store}>
    <PersistGate loading={<h2>Loading app...</h2>} persistor={persistor}>
      <QueryClientProvider client={queryClient}>
        <BrowserRouter>
          <Switch>
            <Route exact={true} path="/" component={Home} />
          </Switch>
        </BrowserRouter>
      </QueryClientProvider>
    </PersistGate>
  </Provider>
);

export default App;
