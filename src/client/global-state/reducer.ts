import { atomicGridReducer } from '../atomic/Grid/state';

export const reducers = {
  atomicGridReducer,
};
