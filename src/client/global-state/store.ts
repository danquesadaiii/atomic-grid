import { createStore, applyMiddleware, Middleware } from 'redux';
import { persistStore } from 'redux-persist';
// import createSagaMiddleware from '@redux-saga/core'

import rootReducer from './root-reducer';
// import { rootSaga } from './index'

export const store = createStore(rootReducer);

export const persistor = persistStore(store);
