import { useQuery } from 'react-query';
import axios from 'axios';

interface GetAppConfigParams {
  refetch?: boolean;
  id: string;
}

export const getAppConfig = ({ refetch, id }: GetAppConfigParams) => {
  console.log('refetch?', refetch);

  return useQuery<any, Error>(
    id,
    async () => {
      const { data } = await axios.get('http://localhost:4001/appconfig');

      return data;
    },
    {
      refetchOnWindowFocus: refetch,
    }
  );

  // return {
  //   data: {},
  //   error: null,
  //   isLoading: false,
  //   isError: false,
  // };
};
