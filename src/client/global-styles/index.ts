import styled from 'styled-components';

export const StyledDiv1 = styled.div`
  padding: 20px;
  margin-top: 20px;
  border: 2px solid #ddd;
`;

export const StyledDiv2 = styled.div`
  padding: 20px;
  margin-top: 20px;
  border: 2px solid #eeb3e4;
`;
