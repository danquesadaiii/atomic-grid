import React from 'react';

import { Grid, Sample } from '../atomic';
import { withProfiler } from '../hoc/withProfiler';

const Home = () => {
  return (
    <div>
      Home Test
      <Grid />
      <Sample />
    </div>
  );
};

export default withProfiler(Home, 'red');
