import React, { Profiler, ComponentType } from 'react';

export const withProfiler =
  (Component: ComponentType, color: string) => (hocProps) =>
    (
      <Profiler
        id={Component.name}
        onRender={(id, phase, dur) => {
          console.log(
            `%c ${id}: `,
            `background: ${color}; color: #fff`,
            phase,
            dur
          );
        }}
      >
        <Component {...hocProps} />
      </Profiler>
    );
