var express = require('express');
var router = express.Router();

const fakeAppConfig = {
  timeout_ms: 60000,
  columns: [
    {
      Header: 'Name',
      columns: [
        {
          Header: 'FIRST Name',
          accessor: 'firstName',
        },
        {
          Header: 'LAST Name',
          accessor: 'lastName',
        },
      ],
    },
    {
      Header: 'Info',
      columns: [
        {
          Header: 'AGE',
          accessor: 'age',
        },
        {
          Header: 'VISITS',
          accessor: 'visits',
        },
        {
          Header: 'STATUS',
          accessor: 'status',
        },
        {
          Header: 'PROFILE Progress',
          accessor: 'progress',
        },
      ],
    },
  ],
};

/* GET home page. */
router.get('/', function (req, res, next) {
  res.render('index', { title: 'Express' });
});

/* ! for spike only */
router.get('/appconfig', function (req, res, next) {
  res.send(fakeAppConfig);
});

module.exports = router;
